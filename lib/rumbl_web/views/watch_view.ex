defmodule RumblWeb.WatchView do
  use RumblWeb, :view

  def video_id("https://youtu.be/" <> id), do: id
  def video_id("http://www.youtube.com/watch?v=" <> id), do: id
  def video_id("http://www.youtube.com/?v=" <> id), do: id
end
