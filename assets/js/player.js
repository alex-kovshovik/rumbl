let Player = {
    player: NodeList,

    init(domId, videoId, onReady) {
        window.onYouTubeIframeAPIReady = () => {
            this.onIframeReady(domId, videoId, onReady)
        }

        let youtubeScriptTag = document.createElement("script")
        youtubeScriptTag.src = "https://www.youtube.com/iframe_api"
        document.head.appendChild(youtubeScriptTag)
    },

    onIframeReady(domId, videoId, onReady) {
        this.player = new YT.Player(domId, {
            height: "360",
            width: "420",
            videoId: videoId,
            host: 'https://www.youtube.com',
            playerVars: { 'autoplay': 0, 'controls': 0, 'autohide':1,'wmode':'opaque','origin': 'http://localhost:4000' },
            events: {
                "onReady": (event => onReady(event)),
                "onStateChange": (event => this.onPlayerStateChange(event))
            }
        })
    },

    onPlayerStateChange(event) {},

    getCurrentTime() { return Math.floor(this.player.getCurrentTime() * 1000) },
    seekTo(millsec) { return this.player.seekTo(millsec / 1000) }
}

export default Player
