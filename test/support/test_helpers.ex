defmodule Rumbl.TestHelpers do
  alias Rumbl.{
    Accounts,
    Multimedia
  }

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        name: "Some User",
        username: username,
        credential: %{
          email: attrs[:email] || "#{username}@example.com",
          password: attrs[:password] || "super-secret"
        }
      })
      |> Accounts.register_user()

    user
  end

  def category_fixture(name \\ "Comedy") do
    Multimedia.create_category(name)
  end

  def video_fixture(%Accounts.User{} = user, attrs \\ %{}) do
    attrs =
      Enum.into(attrs, %{
        title: "A Title",
        url: "http://example.com",
        description: "a description",
        category_id: category_fixture().id
      })

      {:ok, video} = Multimedia.create_video(user, attrs)

      video
  end

  def login(%{conn: conn, login_as: username}) do
    user = user_fixture(username: username)
    {Plug.Conn.assign(conn, :current_user, user), user}
  end
  def login(%{conn: conn}), do: {conn, :logged_out}
end
