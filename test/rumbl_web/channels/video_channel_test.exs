defmodule RumblWeb.Channels.VideoChannelTest do
  use RumblWeb.ChannelCase

  import Rumbl.TestHelpers

  setup do
    user = user_fixture(name: "Gary")
    video = video_fixture(user, title: "Testing")
    token = Phoenix.Token.sign(@endpoint, "user socket", user.id)

    {:ok, socket} = connect(RumblWeb.UserSocket, %{"token" => token})
    {:ok, socket: socket, user: user, video: video}
  end

  test "inserting new annotations", %{socket: socket, video: vid} do
    {:ok, _, socket} = subscribe_and_join(socket, "videos:#{vid.id}", %{})
    ref = push socket, "new_annotation", %{body: "the body", at: 0}
    assert_reply ref, :ok, %{}
    assert_broadcast("new_annotation", %{})
  end
end
